package com.tracer.extract;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tracer.entity.Message;
import com.tracer.entity.Node;
import com.tracer.entity.Root;
import com.tracer.storage.LogWarehouse;
import com.tracer.util.StatsTracker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ExtractorTest {

  List<Node> nodes;
  Extractor extractor;
  Gson gson;
  private LogWarehouse storage;
  private ConcurrentLinkedQueue<Message> messageQ;
  private StatsTracker statsTracker;

  @BeforeEach
  void setUp() {
    nodes = new ArrayList<>();
    Node node1 =
        new Node(
            "trace1",
            "back-end-3",
            "2016-10-20T12:43:34.000Z",
            "2016-10-20T12:43:35.000Z",
            "ac",
            "ad");
    nodes.add(node1);

    Node node2 =
        new Node(
            "trace1",
            "back-end-1",
            "2016-10-20T12:43:33.000Z",
            "2016-10-20T12:43:36.000Z",
            "aa",
            "ac");
    nodes.add(node2);

    Node node3 =
        new Node(
            "trace1",
            "back-end-2",
            "2016-10-20T12:43:38.000Z",
            "2016-10-20T12:43:40.000Z",
            "aa",
            "ab");
    nodes.add(node3);

    Node node4 =
        new Node(
            "trace1",
            "front-end",
            "2016-10-20T12:43:32.000Z",
            "2016-10-20T12:43:42.000Z",
            "null",
            "aa");
    nodes.add(node4);

    storage = new LogWarehouse();
    messageQ = new ConcurrentLinkedQueue<>();
    statsTracker = new StatsTracker();
    extractor = new Extractor(storage, messageQ, statsTracker);
    gson = new GsonBuilder().create();
  }

  @Test
  void testBuildRoot() {
    Root root = extractor.buildRoot(nodes);
    String expectedTrace =
        "{\"id\":\"trace1\",\"root\":{\"service\":\"front-end\",\"start\":\"2016-10-20T12:43:32.000Z\",\"end\":" +
                "\"2016-10-20T12:43:42.000Z\",\"calls\":[{\"service\":\"back-end-1\",\"start\":" +
                "\"2016-10-20T12:43:33.000Z\",\"end\":\"2016-10-20T12:43:36.000Z\",\"calls\":[{\"service\":" +
                "\"back-end-3\",\"start\":\"2016-10-20T12:43:34.000Z\",\"end\":\"2016-10-20T12:43:35.000Z\"," +
                "\"calls\":[],\"span\":\"ad\"}],\"span\":\"ac\"},{\"service\":\"back-end-2\",\"start\":" +
                "\"2016-10-20T12:43:38.000Z\",\"end\":\"2016-10-20T12:43:40.000Z\",\"calls\":[],\"span\":\"ab\"}]," +
                "\"span\":\"aa\"}}";
    assertEquals(expectedTrace, gson.toJson(root));
  }
}
