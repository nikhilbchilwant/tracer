### Problem statement

[Please read the problem statement here.](https://github.com/AI-IshanBhatt/SIMSCALE-Task#simscale-task).

### How to build and run

Build using `mvn package`. It will create the jar `Tracer-0.1-SNAPSHOT.jar`. Now, copy the jar and the `run.sh` from `scripts` folder to `traces-evaluator/bin`. Your directory structure should look like below:

```
user@laptop:~/Downloads/traces-evaluator$ tree
.
├── bin
│   ├── run.sh
│   ├── Tracer-0.1-SNAPSHOT.jar
│   ├── traces-evaluator
│   └── traces-evaluator.bat
└── lib
    ├── 
    ├── 
```

Now run the evaluator using `bash traces-evaluator ./run.sh`.

You can get the `Tracer-0.1-SNAPSHOT.jar` from release directly.

### Demo

[![asciicast](https://asciinema.org/a/hvrgWQDXWXwCHKIltl6Svn8Ry.svg)](https://asciinema.org/a/hvrgWQDXWXwCHKIltl6Svn8Ry)

### Approach

Please see the `design document.pdf` in the `doc`.