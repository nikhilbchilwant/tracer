package com.tracer.extract;

import com.google.common.graph.GraphBuilder;
import com.google.common.graph.Graphs;
import com.google.common.graph.MutableGraph;
import com.google.gson.Gson;
import com.tracer.entity.Message;
import com.tracer.entity.Node;
import com.tracer.entity.Root;
import com.tracer.storage.LogWarehouse;
import com.tracer.util.StatsTracker;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

import static com.tracer.util.Constants.CLEAR;

@Log4j2
public class Extractor implements Runnable {

  private static final String ROOT_SPAN = "null";
  private static final int SLEEP_DURATION = 10;
  private static final int PAUSE_INTERVAL = 100;
  private final StatsTracker statsTracker;
  private LogWarehouse storage;
  private ConcurrentLinkedQueue<Message> messageQ;
  private Gson gson;

  public Extractor(
      @NonNull LogWarehouse storage,
      @NonNull ConcurrentLinkedQueue<Message> messageQ,
      @NonNull StatsTracker statsTracker) {
    this.storage = storage;
    this.messageQ = messageQ;
    this.gson = new Gson();
    this.statsTracker = statsTracker;
  }

  @Override
  public void run() {
    List<List<Node>> nodeLists;
    long counter = 0;

    while (true) {
      try {
        Message message = messageQ.peek();
        if (message != null) {
          // Did not remove message from queue to keep it accessible for other threads
          log.info("Received termination message");
          if (message.getMessage().equals(CLEAR)) {
            nodeLists = storage.fetch(true);
            if (nodeLists.size() != 0) log.info("Fetched {} traces", nodeLists.size());
            buildAndPrintTrace(nodeLists);
            log.info("Terminating thread");
            break;
          }
        }
        nodeLists = storage.fetch();
        if (nodeLists.size() != 0) {
          log.info("Fetched {} traces", nodeLists.size());
          log.info("Current storage size : {} logs", storage.getRecordCount());
        }
        buildAndPrintTrace(nodeLists);
        counter++;
        if (counter % PAUSE_INTERVAL == 0) {
          Thread.sleep(SLEEP_DURATION);
        }

      } catch (Exception e) {
        log.error(e);
        break;
      }
    }
    statsTracker.printSummary();
  }

  private void buildAndPrintTrace(List<List<Node>> nodeLists) {
    for (List<Node> nodes : nodeLists) {
      log.trace("Building root of {} nodes", nodes.size());
      statsTracker.incrementProcessedLogs(nodes.size());
      Root root = buildRoot(nodes);
      if (root != null) print(root);
      else {
        statsTracker.incrementOrphanCount(nodes.size());
      }
    }
  }

  private void print(@NonNull Root root) {
    System.out.print(gson.toJson(root));
  }

  /**
   * Returns root node for the trace
   *
   * @param nodes list of nodes
   * @return root node if the list had valid nodes. Otherwise, returns null.
   */
  public Root buildRoot(@NonNull List<Node> nodes) {
    Node topNode = null;
    try {
      topNode = buildTrace(nodes);

    } catch (IllegalArgumentException traceBuildException) {
      log.error("Failed to build trace JSON for node", traceBuildException);
    }
    if (topNode != null) {
      Root root = new Root(topNode.getTraceID(), topNode);
      log.trace("Successfully built graph with root traceID {}", root.getId());
      return root;
    } else {
      // relying on the storage implementation to give non-empty list of nodes.
      log.trace(
          "Failed to create a valid graph for the node with traceID {}", nodes.get(0).getTraceID());
      return null;
    }
  }

  /**
   * Checks whether input forms a valid graph and adds 'calls' to the nodes.
   *
   * @param nodes list of fetched nodes
   * @return root of the graph
   * @throws IllegalArgumentException if graph is cyclic
   */
  private Node buildTrace(List<Node> nodes) throws IllegalArgumentException {
    HashMap<String, List<Node>> edgeToNodesMap = mapEdgeToNodes(nodes);
    if (checkValidity(edgeToNodesMap)) {
      MutableGraph<Node> graph =
          GraphBuilder.undirected().allowsSelfLoops(false).expectedNodeCount(nodes.size()).build();
      // ExpectedNodeCount ensures that the trace graph forms a single connected component

      // checkValidity ensures that this will not fail
      Node root = edgeToNodesMap.get(ROOT_SPAN).get(0);

      // Add nodes in breadth first traversal way
      Queue<Node> frontierNodes = new LinkedList<>();
      frontierNodes.add(root);
      log.trace("Building graph with root node (traceID : {}}", root.getTraceID());
      while (!frontierNodes.isEmpty()) {
        Node parent = frontierNodes.remove();
        List<Node> childNodes = edgeToNodesMap.get(parent.getSpan());
        if (childNodes != null) {
          for (Node child : childNodes) {
            graph.putEdge(parent, child);
            frontierNodes.add(child);
            parent.addCall(child); // creates call list
          }
        }
      }
      if (Graphs.hasCycle(graph))
        throw new IllegalArgumentException("The given nodes created a cyclic graph");
      return root;
    } else return null;
  }

  /** creates mapping parentSpan -> node. The span becomes edge in graph. */
  private HashMap<String, List<Node>> mapEdgeToNodes(List<Node> nodes) {
    HashMap<String, List<Node>> mapping = new HashMap<>();
    for (Node node : nodes) {
      String parentSpan = node.getParentSpan();
      List<Node> nodesOfEdge = mapping.get(parentSpan);
      if (nodesOfEdge == null) nodesOfEdge = new ArrayList<>();
      nodesOfEdge.add(node);
      mapping.put(parentSpan, nodesOfEdge);
    }
    return mapping;
  }

  private boolean checkValidity(HashMap<String, List<Node>> edgeToNodesMapping) {
    List<Node> rootNode = edgeToNodesMapping.get(ROOT_SPAN);
    // there should be one and only one root node
    return rootNode != null && rootNode.size() == 1;
  }
}
