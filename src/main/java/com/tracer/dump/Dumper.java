package com.tracer.dump;

import com.tracer.entity.Message;
import com.tracer.entity.Node;
import com.tracer.storage.LogWarehouse;
import com.tracer.util.StatsTracker;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.ConcurrentLinkedQueue;

import static com.tracer.util.Constants.CLEAR;

@Log4j2
public class Dumper implements Runnable {

  private final int SLEEP_DURATION = 100;
  private final int PAUSE_INTERVAL = 1000;
  private StatsTracker statsTracker;
  private Scanner traceReader;
  private LogWarehouse storage;
  private ConcurrentLinkedQueue<Message> messageQ;

  public Dumper(@NonNull LogWarehouse storage, @NonNull ConcurrentLinkedQueue<Message> messageQ, @NonNull StatsTracker statsTracker) {
    this.storage = storage;
    this.messageQ = messageQ;
    this.traceReader = new Scanner(System.in);
    this.statsTracker = statsTracker;
  }

  @Override
  public void run() {
    String logLine = "";
    Node node;
    long counter = 1;
    long logCount = 0;

    while (traceReader.hasNextLine()) {
      try {
        logLine = traceReader.nextLine();
        log.trace("Received log line : {}", logLine);
        node = createNode(logLine);
        storage.add(node);
        if (counter % PAUSE_INTERVAL == 0) {
          // sleeping helped to free resources for other threads and avoids heating problem of
          // laptop.
          log.info("Dumped {} logs", storage.getRecentLineNo());
          log.trace("Going to sleep for {}", SLEEP_DURATION);
          Thread.sleep(SLEEP_DURATION);
        }
        counter++;
      } catch (IOException storageFullException) {
        log.error("Error during reading input", storageFullException);
      } catch (IllegalArgumentException invalidLogException) {
        log.info("Ignored invalid log line : {}", logLine);
        statsTracker.incrementMalformedCount();
      } catch (Exception e) {
        log.error(e);
        break;
      }
    }

    log.info("Sending 'CLEAR' message");
    Message cleanupMsg = new Message(CLEAR);
    messageQ.add(cleanupMsg);
    log.info("Terminating thread");
  }

  /**
   * Creates the object from log string
   * @param log string log line
   * @return Nodes
   * @throws IllegalArgumentException if log line is malformed
   */
  public Node createNode(String log) throws IllegalArgumentException {
    String[] dataSplits = log.split("->| ");
    if (dataSplits.length != 6)
      throw new IllegalArgumentException(
          "Malformed log string. Expected to have 6 tokens after .split('->| ')");
    String start = dataSplits[0];
    String end = dataSplits[1];
    String traceID = dataSplits[2];
    String serviceName = dataSplits[3];
    String receivedSpan = dataSplits[4];
    String generatedSpan = dataSplits[5];

    return new Node(traceID, serviceName, start, end, receivedSpan, generatedSpan);
  }
}
