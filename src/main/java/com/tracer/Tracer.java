package com.tracer;

import com.tracer.dump.Dumper;
import com.tracer.entity.Message;
import com.tracer.extract.Extractor;
import com.tracer.storage.LogWarehouse;
import com.tracer.util.StatsTracker;
import lombok.extern.log4j.Log4j2;

import java.util.concurrent.ConcurrentLinkedQueue;

@Log4j2
public class Tracer {
  public static final int EXTRACTOR_COUNT = 1; //number of executor threads. Worked fine for me with single thread.
  private static LogWarehouse warehouse;
  private static Dumper dumper;
  private static Extractor extractor;
  private static Thread dumperThread;
  private static Thread extractorThread;
  private static ConcurrentLinkedQueue<Message> messageQueue;
  private static ThreadGroup dumperGroup, extractorGroup;
  private static StatsTracker statsTracker;

  public static void main(String[] args) {

    warehouse = new LogWarehouse();
    messageQueue = new ConcurrentLinkedQueue<>();
    statsTracker = new StatsTracker();

    // Dumper threads
    dumperGroup = new ThreadGroup("Dumpers");
    dumper = new Dumper(warehouse, messageQueue, statsTracker);
    dumperThread = new Thread(dumperGroup, dumper, "Dumper");
    log.info("Starting the dumper thread");
    dumperThread.start();

    // extractor threads
    extractorGroup = new ThreadGroup("Extractors");
    for (int threadNo = 1; threadNo <= EXTRACTOR_COUNT; threadNo++) {
      extractor = new Extractor(warehouse, messageQueue, statsTracker);
      String threadName = "Extractor-" + threadNo;
      extractorThread = new Thread(extractorGroup, extractor, threadName);
      extractorThread.start();
      log.info("Starting the extractor thread #{}", threadNo);
    }
    statsTracker.printSummary();
  }
}
