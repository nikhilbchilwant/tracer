package com.tracer.util;

import lombok.extern.log4j.Log4j2;

import java.util.concurrent.atomic.AtomicInteger;

/** Tracks statistics related to application */
@Log4j2
public class StatsTracker {
  private AtomicInteger orphanCount;
  private AtomicInteger malformedCount;
  private AtomicInteger totalProcessedLogs;

  public StatsTracker() {
    this.orphanCount = new AtomicInteger(0);
    this.malformedCount = new AtomicInteger(0);
    this.totalProcessedLogs = new AtomicInteger(0);
  }

  public synchronized void incrementOrphanCount(int increment) {
    orphanCount.addAndGet(increment);
  }

  public synchronized void incrementMalformedCount() {
    malformedCount.incrementAndGet();
  }

  public synchronized void incrementProcessedLogs(int increment) {
    totalProcessedLogs.addAndGet(increment);
  }

  public void printSummary() {
    log.info(
        "Orphan log count : {}, Malformed log count : {}, Total processed logs : {}",
        orphanCount.get(),
        malformedCount.get(),
        totalProcessedLogs.get());
  }
}