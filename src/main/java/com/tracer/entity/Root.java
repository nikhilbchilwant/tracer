package com.tracer.entity;

import lombok.Getter;

/**
 * Root node for trace graph
 */
public class Root {

    @Getter private String id;

    private Node root;

    public Root(String id, Node root) {
        this.id = id;
        this.root = root;
    }
}