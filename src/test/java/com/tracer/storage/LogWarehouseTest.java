package com.tracer.storage;

import com.tracer.entity.Node;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class LogWarehouseTest {

  Node node;
  LogWarehouse warehouse;

  @BeforeEach
  void setUp() {
    node =
        new Node(
            "trace1",
            "back-end-3",
            "2016-10-20T12:43:34.000Z",
            "2016-10-20T12:43:35.000Z",
            "ac",
            "ad");
    warehouse = new LogWarehouse();
  }

  @Test
  void testAddingNode() throws IOException {
    assertTrue(warehouse.add(node));
    assertEquals(1, warehouse.getRecordCount());
  }

  @Test
  void testFetchingNodes() throws IOException {
    for (long lineNo = 1; lineNo <= 10; lineNo++) {
      warehouse.add(node);
    }

    Node node2 =
        new Node(
            "trace2",
            "back-end-3",
                "2016-10-20T12:43:34.000Z",
                "2016-10-20T12:43:35.000Z",
            "ac",
            "ad");
    for (long lineNo = 11; lineNo <= 111; lineNo++) {
      // 111 so that (111 - STALE_COUNT) is 10
      warehouse.add(node2);
    }

    assertEquals(10, warehouse.fetch().get(0).size());
    assertEquals(101, warehouse.getRecordCount()); // we inserted 111 and fetched 10.
    assertEquals(111, warehouse.getRecentLineNo());
  }

  @Test
  void testFetchAll() throws IOException {
    for (long lineNo = 1; lineNo <= 10; lineNo++) {
      warehouse.add(node);
    }
    assertEquals(10, warehouse.fetch(true).get(0).size());
  }
}
