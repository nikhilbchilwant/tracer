package com.tracer.entity;

import lombok.Getter;

/**
 * Message for communication between threads.
 */
public class Message {

    @Getter private String message;

    public Message(String message) {
        this.message = message;
    }
}
