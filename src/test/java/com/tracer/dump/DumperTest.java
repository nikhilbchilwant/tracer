package com.tracer.dump;

import com.tracer.entity.Message;
import com.tracer.entity.Node;
import com.tracer.storage.LogWarehouse;
import com.tracer.util.StatsTracker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ConcurrentLinkedQueue;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DumperTest {

  String logLine;
  Dumper dumper;

  @BeforeEach
  void setUp() {
    logLine = "2016-10-20T12:43:34.000Z 2016-10-20T12:43:35.000Z trace1 back-end-3 ac->ad";
    LogWarehouse warehouse = new LogWarehouse();
    ConcurrentLinkedQueue<Message> messageQueue = new ConcurrentLinkedQueue<>();
    StatsTracker statsTracker = new StatsTracker();
    dumper = new Dumper(warehouse, messageQueue, statsTracker);
  }

  @Test
  public void testNodeCreation() {
    Node expectedNode =
        new Node(
            "trace1",
            "back-end-3",
            "2016-10-20T12:43:34.000Z",
            "2016-10-20T12:43:35.000Z",
            "ac",
            "ad");
    assertEquals(expectedNode, dumper.createNode(logLine));
  }
}
