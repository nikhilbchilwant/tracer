package com.tracer.storage;

import com.tracer.entity.Node;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListMap;

/** Data storage for logs */
@Log4j2
public class LogWarehouse {

  private static final long MAX_CAPACITY = 1000000; // given in the document
  private static final long STALE_COUNT =
      100; // lines with line no. lesser than lineCount - STALE_COUNT are ready for pruning
  @Getter private long recentLineNo; // always increases
  private Hashtable<String, List<Node>>
      nodeStorage; // Stores traceID and corresponding list of Nodes.
  private ConcurrentSkipListMap<Long, String>
      freshnessTracker; // Stores line no. and corresponding traceID. We measure freshness of a
                        // trace by line no.
  private Hashtable<String, Long>
      lineTracker; // stores trace ID and line no. corresponding to the last received line no.
  @Getter private long recordCount; // gives idea about current storage size

  public LogWarehouse() {
    this.nodeStorage = new Hashtable<>();
    //    this.freshnessTracker = Collections.synchronizedSortedMap(new TreeMap<>());
    this.freshnessTracker = new ConcurrentSkipListMap<>();
    this.lineTracker = new Hashtable<>();
    this.recordCount = 0;
    this.recentLineNo = 0;
  }

  /**
   * Adds a node to the storage if there is sufficient space.
   *
   * @param node Node object created using the log line
   * @throws IOException if storage is full
   */
  public synchronized boolean add(Node node) throws IOException {

    if (!isHealthy()) throw new IOException("Storage is full");

    String traceID = node.getTraceID();

    // If the key should be either present or absent everywhere at any point of time.
    if ((nodeStorage.containsKey(traceID) ^ lineTracker.containsKey(traceID))) {
      throw new IOException("Inconsistent storage");
    }
    log.trace("Adding node with traceID {} to the LogWarehouse.", traceID);
    boolean addedToNodeStorage = addToNodeStorage(node, traceID);

    boolean addedToTrackers = addToTrackers(recentLineNo + 1, traceID);

    //Cautious when dealing with concurrency
    if (addedToNodeStorage && addedToTrackers) {
      recentLineNo++;
      recordCount++;
      log.trace("Added node with traceID {}. Record count is {}", traceID, recordCount);
    }
    return true;
  }

  // add node to the line tracker and freshness tracker
  private synchronized boolean addToTrackers(Long lineNo, String traceID) {
    log.trace("Adding node to the tracker.");
    if (lineTracker.containsKey(traceID)) {
      freshnessTracker.remove(lineTracker.get(traceID));
    }
    lineTracker.put(traceID, lineNo); // replaces old record
    freshnessTracker.put(lineNo, traceID);
    log.trace("Successfully added node to the tracker.");
    return true;
  }

  // append node to the node storage list
  private synchronized boolean addToNodeStorage(Node node, String traceID) {
    log.trace("Adding node to the node storage");
    List<Node> nodes = nodeStorage.get(traceID);

    if (nodes == null) {
      nodes = new ArrayList<>();
    }
    nodes.add(node);
    nodeStorage.put(traceID, nodes);

    log.trace("Successfully added node to the node storage.");
    return true;
  }

  private boolean isHealthy() {
    return this.recordCount <= MAX_CAPACITY;
  }

  /**
   * Fetch stale traces i.e. traces which haven't got any corresponding log line for the last 100
   * (STALE_LIMIT) lines.
   *
   * @return List of list of nodes. one list per traceID.
   */
  public synchronized List<List<Node>> fetch() {
    return fetch(false);
  }

  /**
   * @param fetchRemaining to ignore staleness criteria for traces (STALE_LIMIT)
   * @return all remaining traces
   */
  public synchronized List<List<Node>> fetch(boolean fetchRemaining) {
    long lineLimit = recentLineNo - STALE_COUNT;
    if (lineLimit < 0) lineLimit = 0; // Initially we have to wait till storage grows beyond 100.
    if (fetchRemaining) lineLimit = Integer.MAX_VALUE;

    // lines lesser than this lineNo are ready to be processed.
    log.trace("Fetching all logs with lineNo. less than {}", lineLimit);
    return prune(lineLimit);
  }

  /**
   * Remove traces older than line limit.
   *
   * @param lineLimit line no. below which traces will be pruned
   * @return stale nodes
   */
  private synchronized List<List<Node>> prune(long lineLimit) {
    List<List<Node>> prunedNodes = new ArrayList<>();
    SortedMap<Long, String> nodesToPrune =
        Collections.synchronizedSortedMap(freshnessTracker.headMap(lineLimit));

    for (Map.Entry<Long, String> traceLife : Collections.synchronizedSet(nodesToPrune.entrySet())) {
      String traceID = traceLife.getValue();
      Long lineNo = traceLife.getKey();
      List<Node> node = nodeStorage.get(traceID);
      recordCount = recordCount - node.size();
      prunedNodes.add(node);

      // remove all corresponding records
      lineTracker.remove(traceID);
      freshnessTracker.remove(lineNo);
      nodeStorage.remove(traceID);
    }
    return prunedNodes;
  }
}
